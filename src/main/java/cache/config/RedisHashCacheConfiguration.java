package cache.config;


import cache.domain.Constant;
import cache.rediscache.RedisHashCache;
import cache.rediscache.RedisHashCacheManager;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;


/**
 * @description: 配置RedisHashCacheManage
 * @author: zhangzc
 * @modified By: zhangzc
 * @date: Created in 2022/7/13 12:02
 * @version:v1.0
 */
@Configuration
@ConditionalOnProperty("cache.enable")
@EnableConfigurationProperties(RedisManagerProperties.class)
public class RedisHashCacheConfiguration {

    @Autowired
    private RedisManagerProperties redisManagerProperties;

    public RedisHashCacheConfiguration(DefaultListableBeanFactory factory) {
        factory.getBeanDefinition(RedisHashCacheManager.BEAN_NAME).setAutowireCandidate(false);
    }

    /**
     * 自定义hash cacheManage
     * <p>
     * 使用此manager
     * </p>
     *
     * @return CacheManager
     */
    @Bean(RedisHashCacheManager.BEAN_NAME)
    public CacheManager redisHashCacheManager(GenericToStringSerializer<String> genericToStringSerializer,
                                              GenericJackson2JsonRedisSerializer genericJsonRedisSerializer) {

        RedisHashCacheManager cacheManager = new RedisHashCacheManager();
        List<RedisHashCache> caches = new ArrayList<>(1);

        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
            .entryTtl(Duration.ofSeconds(redisManagerProperties.getTtlDuration()))
            .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(genericToStringSerializer))
            .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(genericJsonRedisSerializer));

        /**
         * @see RedisCacheManager#getMissingCache(String)
         */
        // 手动添加缓存名 如果要使用这个cacheManger 每次加一个cacheName都需要在这里加上  todo 可以优化
        caches.add(new RedisHashCache(Constant.CONSTRAINTS_TEMPLATE_PREFIX, redisCacheConfiguration));
        cacheManager.setCaches(caches);

        return cacheManager;
    }

    /**
     * 指定 @Cacheable默认的manager
     */
    @Bean
    @Primary
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory, GenericToStringSerializer<String> genericToStringSerializer,
                                     GenericJackson2JsonRedisSerializer genericJsonRedisSerializer) {

        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory);

        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
            .entryTtl(Duration.ofSeconds(redisManagerProperties.getTtlDuration()))
            .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(genericToStringSerializer))
            .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(genericJsonRedisSerializer));

        return new RedisCacheManager(redisCacheWriter, redisCacheConfiguration);
    }

    @Bean
    public GenericToStringSerializer<String> genericToStringSerializer() {
        return new GenericToStringSerializer<>(String.class);
    }

    @Bean
    public GenericJackson2JsonRedisSerializer genericJsonRedisSerializer() {
        ObjectMapper redisJsonMapper = Jackson2ObjectMapperBuilder.json().serializationInclusion(JsonInclude.Include.NON_NULL)
            .failOnUnknownProperties(false).featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS).build();
        // 把类名作为属性进行存储
        redisJsonMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        // 剔除类上的所有注解
        redisJsonMapper.configure(MapperFeature.USE_ANNOTATIONS, false);
        redisJsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        // 枚举类使用toString方法进行反序列化
        redisJsonMapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
        // 枚举类使用toString方法进行反序列化
        redisJsonMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
        return new GenericJackson2JsonRedisSerializer(redisJsonMapper);
    }

}
