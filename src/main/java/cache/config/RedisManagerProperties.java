package cache.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "cache-hash")
public class RedisManagerProperties {

    /**
     * 实体缓存有效期，单位 秒，默认7200秒，即2小时
     */
    private int ttlDuration = 7200;

    public int getTtlDuration() {
        return ttlDuration;
    }

    public void setTtlDuration(int ttlDuration) {
        this.ttlDuration = ttlDuration;
    }
}
