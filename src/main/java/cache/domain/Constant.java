package cache.domain;

/**
 * @author：zzc
 * @date: 2022/7/27
 */
public class Constant {

    public final static String CHAR_SET = "UTF-8";

    public static final String DOUBLE_COLON = "::";

    public static final String COLON = ":";

    public static final String ASTERISK = "*";

    public static final String CONSTRAINTS_TEMPLATE_PREFIX = "constraints::templateId";
}
